- title : Functional Web with Suave
- description : Functional Web Development with F# and Suave
- author : Stefano Paluello
- theme : moon
- transition : default

***

## Web as a Function


<br />


### The Suave approach


<br />


Stefano Paluello - [@palutz](http://www.twitter.com/palutz)


***


### How is working the Web?

<br />
<br />

    Request -> [ SERVER ] -> Response

<br />

_or_  ( λ approach )

<br />

    Request -> [ Function ] -> Response


---

### Functional Web using F#
<br />

> Pseudocode


    type Request = Request of string

    type Response = Response of string

    let webServer (req: Request) : Response = 
      req |> map(fun r -> createResponse)
    

---


### Functional Web using F#
<br />

> F# code

    namespace MySuave
    module Http =
      type RequestType = GET | POST 

      type Request = {
        Route : string
        ReqType: RequestType
      }

      type Response = {
        Content : string
        StatusCode : int
      }


---

### Functional Web using F#
<br />

> F# code
    
    type Context = {
      Request : Request
      Response : Response
    }

    let webServer (ctx: Context) = 
      ...
      let newcontent = ...
      { ctx with Response = {Content = newcontent; StatusCode = 200 }}
      

--- 

### More accurate webserver 
<br />

Instead of just 

    Request -> [ Function ] -> Response


A webserver answers asynchronously


    Request -> [Function ] -> Async<Response>

    // possible F# signature
    Request -> Async<Response>



---

### More accurate webserver 
<br />

And also sometimes we can get no answer at all


    Request -> [ Function ] -> Async<[Response | No Response ]

    // possible F# signature
    Request -> Async<Response option>

    // Remember from your F# knowledge 
    type Option<'T> =
      | Some of 'T
      | None


***

### Introducing WebPart 
<br />

- We are actually exchanging contexts


    Context -> Async<Context option>


- We can define a new type: a WebPart


    type WebPart = Context -> Async<Context option>
    // Mark this type
    // This is the base type in the Suave design

---

### Our first Webpart
<br />

- Remeber that a webpart is defined as following


    Context -> Async<Context option>


- now we define our first webpart

  
    // OK Webpart - the happy path :-) 
    let OK (content : string) (context : Context) =
      { context with Response = {Content = content; StatusCode = 200 }}
      |> Some
      |> async.Return

    // signature
    string -> Context -> Async<Context option>

---

### Execute Webparts
<br />

    let execute inputCtx webpart =
      async {
        let! outputCtx = webpart inputCtx
        match outputCtx with
        | Some c ->
              // we have some context. Let's print it out
              printfn "Code : %d" c.Response.StatusCode
              printfn "Output : %s" c.Response.Content
        | None -> printfn "No Output"
      } |> Async.RunSynchronously

---

### Execute Webparts

> Run your webserver in a console


    [<EntryPoint>]
    let main argv =
      let req = {Route = ""; ReqType = Http.GET}
      let resp = {Content = ""; StatusCode = 200}
      let ctx = {Request = req; Response = resp }

      execute ctx (OK "Hello MySuave!")
      0


    // With result:

    Code : 200
    Output : Hello MySuave!


***

### Compose Webparts

- Two webparts, if the 1st is successful, we execute the 2nd otherwise, we return None


    // execute the 1st one
    async {
      let! outputContextOfFirst = first context
    }


    // execute the 1st and check if failed
    async {
      let! outputContextOfFirst = first context
      match outputContextOfFirst with
      | None -> return None
    }

---

### Compose Webparts

> More complete code of composing 2 webparts

    // execute the 1st and if successful execute the 2nd    
    async {
      let! outputContextOfFirst = first context
      match outputContextOfFirst with
      | None -> return None
      | Some context ->
          let outputContextOfSecond = second context
          return! outputContextOfSecond
    }


---

### Introduce compose
<br />

    module Combinators =

      // WebPart -> WebPart -> WebPart
      let compose first second context = async {
        let! firstContext = first context
        match firstContext with
        | None -> return None
        | Some context ->
        let! secondContext = second context
        return secondContext
      }


      let (>=>) = compose


---

### Using Compose

> A simple example how to compose webparts


    (OK "hello") >=> (OK "World!")


    // With output
    Code : 200
    Output : World!
